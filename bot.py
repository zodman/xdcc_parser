#!/usr/bin/env python
# _*_ coding:utf-8 _*_
import sys
import guessit
import re

re_str = [
    r"^\#(?P<id>\d+)\s*(?P<down>[\d])x\s*\[(?P<size>[\d\.(M|G|MG|MB)]*)\]\s*(?P<file>.*)",
    r"^\#(?P<id>\d+)\s+\[(?P<size>[\d\.(M|G|MG|MB)]*)\]\s*(?P<file>.*)"
]


def main(filename):
    f_open = open(filename)
    lines = f_open.readlines()
    for i in lines:
        xdcc_line = i.decode("utf-8").strip()
        for j in re_str:
            res = re.match(j, xdcc_line)
            if res:
                id = res.group("id")
                if "download" in res.group():
                    download = res.group("download")
                else:
                    download = 0
                size = res.group("size")
                file = res.group("file")
                data = guessit.guessit(file)
                data.update({'_file':file,'_download':download, '_size':size})
                print data

if __name__ == "__main__":
    main(sys.argv[1])
